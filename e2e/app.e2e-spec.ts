import { AngularOpdrachtenPage } from './app.po';

describe('angular-opdrachten App', function() {
  let page: AngularOpdrachtenPage;

  beforeEach(() => {
    page = new AngularOpdrachtenPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

console.log('loading libraries');
var express = require('express');
var cors = require('cors');
var app = express();
var http = require('http').Server(app);
var bodyParser = require('body-parser');

(function StockService() {
  initHttpServer();
})();

function initHttpServer() {
  console.log('starting http on 3004');
  app.use(cors());
  app.use(bodyParser.json());

  app.listen('3004', function () {
    console.log('started http on 3004');

  });

  app.get('/overzicht', handleOverzicht);
  function handleOverzicht(request, response) {
    var top5 = overzicht.concat([]);
    top5.forEach(function (entry) {

      // entry.leenHistorie = entry.leenHistorie
      //   .sort(function (a, b) {
      //     return a.datum < b.datum ? 1 : a.datum > b.datum ? -1 : 0
      //   });
      //
      // if (entry.leenHistorie.length > 5) {
      //   entry.leenHistorie = entry.leenHistorie.slice(0, 5);
      // }
    });

    response.json(top5);
  }

  app.get('/detail/:id', handleDetail);
  function handleDetail(request, response) {
    var detail = overzicht.find(function (entry) {
      return entry.id == request.params.id
    });

    // detail && (detail.leenHistorie = detail.leenHistorie.sort(function (a, b) {
    //   return a.datum < b.datum ? 1 : a.datum > b.datum ? -1 : 0
    // }));

    response.json(detail);
  }

  app.post('/store', handleStore);
  function handleStore(request, response) {
    console.log('store reached');
    if (request.body.titel && request.body.auteur) {
      let boek = {
        id: "10000" + (overzicht.length + 1),
        titel: request.body.titel,
        auteur: request.body.auteur,
        img: "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg",
        genre: request.body.genre,
        prijs: request.body.prijs
      };

      overzicht.push(boek);
      console.log(`store pushed ${request.body.titel}`);

      response.json({success: true, request: boek})

    } else {
      response.json({success: false, request: request.body})

    }
  }
}


var overzicht = [
  {
    "id": 100004,
    "titel": "Keyword Mastery",
    "auteur": "O'Rly?",
    "genre": "misc",
    "img": "assets/google.jpg",
    "prijs": 9.95,
    "uitgeleend": false
  },
  {
    "id": 100005,
    "titel": "Refactoring 2.0",
    "auteur": "O'Rly?",
    "genre": "programming",
    "img": "assets/ignore.jpg",
    "prijs": 14.95,
    "uitgeleend": false
  },
  {
    "id": 100006,
    "titel": "Bigger Data",
    "auteur": "O'Rly?",
    "genre": "programming",
    "img": "assets/such-data.jpg",
    "prijs": 11.95,
    "uitgeleend": false
  },
  {
    "id": 100007,
    "titel": "Application Design",
    "auteur": "O'Rly?",
    "genre": "programming",
    "img": "assets/app-design.jpg",
    "prijs": 18.95,
    "uitgeleend": false
  }
];

export interface Boek {
  id: number;           // getal
  titel: string;        // text
  auteur: string;       // text
  genre: string;        // text
  prijs: number;        // number
  img: string;          // text
  uitgeleend: boolean;  // boolean
}
